use std::{
    collections::HashSet,
    io::Write,
    ops::{Deref, Index}, path::Path,
};

use image::{Rgb, RgbImage};
use imageproc::{drawing, rect::Rect};
use rand::seq::SliceRandom;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

#[derive(Debug)]
pub struct Cell {
    pub location: Point,
    pub north: bool,
    pub east: bool,
    pub south: bool,
    pub west: bool,
}

impl Cell {
    pub fn new(location: Point) -> Cell {
        Cell {
            location,
            north: true,
            east: true,
            south: true,
            west: true,
        }
    }
}

#[derive(Debug)]
pub struct Grid {
    pub cells: Vec<Cell>,
    width: usize,
    height: usize,
    visited: HashSet<Point>,
    back_track: Vec<Point>,
}

impl Index<usize> for Grid {
    type Output = Cell;

    fn index(&self, idx: usize) -> &Cell {
        return &self.cells[idx];
    }
}

impl Grid {
    pub fn new(width: usize, height: usize) -> Grid {
        let mut cells: Vec<Cell> = Vec::new();
        for i in 0..(width * height) {
            let x = i % width;
            let y = i / width;
            cells.push(Cell::new(Point { x, y }));
        }
        Grid {
            cells,
            width,
            height,
            visited: HashSet::with_capacity(height * width),
            back_track: Vec::with_capacity(height * width),
        }
    }

    pub fn get_neighbors(&self, location: Point) -> Vec<Point> {
        // Returns a Vector of Points representing the NESW neighbors
        let mut neighbors: Vec<Point> = Vec::with_capacity(4);

        // Check if desired point is outside the bounds
        if location.x >= self.width || location.y >= self.height {
            return neighbors;
        }

        // North Neighbor
        if location.y > 0 {
            neighbors.push(Point {
                x: location.x,
                y: location.y - 1,
            })
        }

        // East Neighbor
        if location.x < self.width - 1 {
            neighbors.push(Point {
                x: location.x + 1,
                y: location.y,
            })
        }

        //South Neighbor
        if location.y < self.height - 1 {
            neighbors.push(Point {
                x: location.x,
                y: location.y + 1,
            })
        }
        //West Neighbor
        if location.x > 0 {
            neighbors.push(Point {
                x: location.x - 1,
                y: location.y,
            })
        }

        neighbors
    }

    pub fn get_unvisited_neighbors(&self, location: Point) -> Vec<Point> {
        let unvisited = self
            .get_neighbors(location)
            .iter()
            .filter(|loc| !self.visited.contains(loc))
            .cloned()
            .collect();

        unvisited
    }


    pub fn save(&self, filename: &str, cell_size: u32, margin: u32) {
        let filename = Path::new(&filename);

        let left_margin = margin;
        let top_margin = margin;
        let image_width = cell_size * self.width as u32 + left_margin * 2;
        let image_height = cell_size * self.height as u32 + top_margin * 2;

        let wall_color = Rgb([255, 255, 255]);
        let background_color = Rgb([0, 0, 0]);
        let mut image = RgbImage::from_pixel(image_width, image_height, background_color);

        for cell in &self.cells {
            if cell.north {
                let x = left_margin + (cell.location.x as u32 * cell_size);
                let y = top_margin + (cell.location.y as u32 * cell_size);
                let width = cell_size;
                let height = 2;
                drawing::draw_filled_rect_mut(&mut image, Rect::at(x as i32, y as i32).of_size(width, height), wall_color);
            }
            if cell.south {
                let x = left_margin + (cell.location.x as u32 * cell_size);
                let y = top_margin + (cell.location.y as u32 * cell_size) + cell_size;
                let width = cell_size;
                let height = 2;
                drawing::draw_filled_rect_mut(&mut image, Rect::at(x as i32, y as i32).of_size(width, height), wall_color);
            }
            if cell.east {
                let x = left_margin + (cell.location.x as u32 * cell_size) + cell_size;
                let y = top_margin + (cell.location.y as u32 * cell_size);
                let width = 2;
                let height = cell_size as u32;
                drawing::draw_filled_rect_mut(&mut image, Rect::at(x as i32, y as i32).of_size(width, height), wall_color);
            }
            if cell.west {
                let x = left_margin + (cell.location.x as u32 * cell_size);
                let y = top_margin + (cell.location.y as u32 * cell_size);
                let width = 2;
                let height = cell_size as u32;
                drawing::draw_filled_rect_mut(&mut image, Rect::at(x as i32, y as i32).of_size(width, height), wall_color);
            }
        }
    image.save(filename).unwrap();

    }


    pub fn carve(&mut self, location: Point) -> Option<Point>{
        if !self.visited.contains(&location) {
            self.visited.insert(location);
            self.back_track.push(location);
        }
        let unvisited_neighbors = self.get_unvisited_neighbors(location);
        let next_location = unvisited_neighbors.choose(&mut rand::thread_rng());
        match next_location {
            Some(next_location) => {
                // Join the two cells

                match location.x == next_location.x {
                    true => {
                        // Next cell is in the same column, check row
                        match location.y > next_location.y {
                            true => {
                                let mut next_cell = self
                                    .cells
                                    .get_mut(next_location.y * self.width + next_location.x)
                                    .unwrap();
                                next_cell.south = false;

                                let current_cell = self
                                    .cells
                                    .get_mut(location.y * self.width + location.x)
                                    .unwrap();
                                current_cell.north = false;
                            } // Next cell is to the north
                            false => {
                                let mut next_cell = self
                                    .cells
                                    .get_mut(next_location.y * self.width + next_location.x)
                                    .unwrap();
                                next_cell.north = false;

                                let current_cell = self
                                    .cells
                                    .get_mut(location.y * self.width + location.x)
                                    .unwrap();
                                current_cell.south = false;
                            } // Next cell is to the south
                        }
                    }
                    false => {
                        // Next cell is in the same row, check column
                        match location.x > next_location.x {
                            true => {
                                let mut next_cell = self
                                    .cells
                                    .get_mut(next_location.y * self.width + next_location.x)
                                    .unwrap();
                                next_cell.east = false;

                                let current_cell = self
                                    .cells
                                    .get_mut(location.y * self.width + location.x)
                                    .unwrap();
                                current_cell.west = false;
                            } // Next cell is to the west
                            false => {
                                let mut next_cell = self
                                    .cells
                                    .get_mut(next_location.y * self.width + next_location.x)
                                    .unwrap();
                                next_cell.west = false;

                                let current_cell = self
                                    .cells
                                    .get_mut(location.y * self.width + location.x)
                                    .unwrap();
                                current_cell.east = false;
                            } // Next cell is to the east
                        }
                    }
                }
                // Perform next iteration
                Some(*next_location)
            }
            None => {
                let next_cell = self.back_track.pop();
                match next_cell {
                    Some(next_cell) => Some(next_cell),
                    None => None,
                }
            }
        }
    }
}

impl Deref for Grid {
    type Target = Vec<Cell>;

    fn deref(&self) -> &Self::Target {
        &self.cells
    }
}

pub trait Draw {
    fn draw(&self);
}

impl Draw for Cell {
    fn draw(&self) {
        let x = self.location.x * 4 + 1;
        let y = self.location.y * 3 + 1;
        // Go to correct location
        print!("\x1B[{};{}H", y, x);
        print!("┌{}┐", if self.north { "──" } else { "  " });
        print!("\x1B[{};{}H", y + 1, x);
        print!(
            "{}  {}",
            if self.west { "│" } else { " " },
            if self.east { "│" } else { " " }
        );
        print!("\x1B[{};{}H", y + 2, x);
        print!("└{}┘", if self.south { "──" } else { "  " });
        std::io::stdout().flush().unwrap();
    }
}

impl Draw for Grid {
    fn draw(&self) {
        // Clear screen
        print!("\x1B[2J");
        std::io::stdout().flush().unwrap();

        for cell in self.iter() {
            cell.draw();
        }
        println!("");
    }
}
