#![recursion_limit = "600"]
use std::{env, process};

use crate::grid::{Draw, Grid, Point};

mod grid;

struct Config {
    height: usize,
    width: usize,
    format: String,
}

impl Config {
    fn build(args: &[String]) -> Result<Config, &'static str> {
        if args.len() == 1 {
            return Ok(Config {
                width: 100,
                height: 50,
                format: "png".to_string(),
            });
        }
        if args.len() != 3 && args.len() != 4 {
            return Err("wrong number of arguments (should be 2 or 3)");
        }
        let width = match args[1].parse::<usize>() {
            Ok(width) => if width > 0 {width} else { 100 },
            Err(_err) => return Err("cannot parse width argument to a usize"),
        };
        let height = match args[2].parse::<usize>() {
            Ok(height) => if height > 0 {height} else { 50 },
            Err(_err) => return Err("cannot parse height argument to a usize"),
        };

        let mut format = "png".to_string();
        if args.len() == 4 {
            if args[3].contains("ascii") || args[3].contains("png") {
                format = args[3].to_string();
            }
        }
        Ok(Config {
            width,
            height,
            format,
        })
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let config = Config::build(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {err}");
        process::exit(1);
    });

    run(config);
}

fn run(config: Config) {
    let mut grid: Grid = Grid::new(config.width, config.height);
    grid.cells[0].north = false;
    grid.cells.last_mut().unwrap().south = false;
    let mut next_location = Point {
        x: config.width / 2,
        y: config.height / 2,
    };
    loop {
        next_location = match grid.carve(next_location) {
            Some(next_location) => next_location,
            None => break,
        };
    }
    grid.carve(Point {
        x: config.width / 2,
        y: config.height / 2,
    });
    if config.format == "ascii" {
        grid.draw();
    } else if config.format == "png" {
        grid.save("maze.png", 25, 25);
    }
}
